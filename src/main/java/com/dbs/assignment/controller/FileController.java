package com.dbs.assignment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dbs.assignment.domain.FileDetail;
import com.dbs.assignment.domain.FileSummary;
import com.dbs.assignment.service.FileService;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/**
 * Controller class that Exposes the file related APIs
 * @author nischaltopno
 *
 */

@RestController
@RequestMapping("file")
@CrossOrigin(origins = "http://localhost:3000")
@Slf4j
public class FileController {

	@Autowired
	private FileService fileService;

	/**
	 * REST API to retrieve the list of files and folders recursively.
	 * 
	 * @param dir
	 *        The directory for which the contents to be retrieved
	 * @return <code>FileSummary</code>
	 *         JSON containing the list of files and folders
	 * @throws Exception
	 */
	@GetMapping("list")
	public Mono<FileSummary> list(@RequestParam(required = true) String dir) throws Exception {
		log.info("method get list start");

		return Mono.just(fileService.getFiles(dir));
	}

	/**
	 * REST API to get the details of a file
	 * 
	 * @param file
	 *        The file for which details needs to be retrieved
	 * @return <code>FileDetail</code>
	 *         The details of the file
	 * @throws Exception
	 */
	@GetMapping("detail")
	public Mono<FileDetail> detail(@RequestParam(required = true) String file) throws Exception {
		log.info("method get detail start");

		return Mono.just(fileService.getFileDetail(file));
	}

}
