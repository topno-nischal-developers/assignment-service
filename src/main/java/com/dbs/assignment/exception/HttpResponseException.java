package com.dbs.assignment.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class HttpResponseException extends ResponseStatusException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public HttpResponseException(HttpStatus status, String message) {
        super(status, message);
    }

    public HttpResponseException(HttpStatus status, String message, Throwable e) {
        super(status, message, e);
    }
}
