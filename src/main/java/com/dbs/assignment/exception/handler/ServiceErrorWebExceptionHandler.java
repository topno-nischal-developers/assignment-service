package com.dbs.assignment.exception.handler;

import java.util.Map;

import org.springframework.boot.autoconfigure.web.ResourceProperties;
import org.springframework.boot.autoconfigure.web.reactive.error.AbstractErrorWebExceptionHandler;
import org.springframework.boot.web.reactive.error.ErrorAttributes;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.codec.ServerCodecConfigurer;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/**
 * Class to handle the exception and prepare response object for the client
 * 
 * @author nischaltopno
 *
 */

@Component
@Order(-2)
@Slf4j
public class ServiceErrorWebExceptionHandler extends AbstractErrorWebExceptionHandler {

	public ServiceErrorWebExceptionHandler(ServiceErrorAttributes g, ApplicationContext applicationContext,
			ServerCodecConfigurer serverCodecConfigurer) {
		super(g, new ResourceProperties(), applicationContext);
		super.setMessageWriters(serverCodecConfigurer.getWriters());
		super.setMessageReaders(serverCodecConfigurer.getReaders());
	}

	@Override
	protected RouterFunction<ServerResponse> getRoutingFunction(final ErrorAttributes errorAttributes) {
		return RouterFunctions.route(RequestPredicates.all(), this::renderErrorResponse);
	}

	private Mono<ServerResponse> renderErrorResponse(final ServerRequest request) {

		final Map<String, Object> errorPropertiesMap = getErrorAttributes(request, false);

		int httpStatusCode = Integer.parseInt(errorPropertiesMap.get("status").toString());
		log.error("status code for response: {}", httpStatusCode);
		HttpStatus httpStatus = HttpStatus.valueOf(httpStatusCode);
		return ServerResponse.status(httpStatus).contentType(MediaType.APPLICATION_JSON)
				.body(BodyInserters.fromValue(errorPropertiesMap));
	}

}