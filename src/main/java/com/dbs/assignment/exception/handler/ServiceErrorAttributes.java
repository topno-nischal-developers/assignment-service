package com.dbs.assignment.exception.handler;

import java.util.Map;

import org.springframework.boot.web.reactive.error.DefaultErrorAttributes;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.server.ResponseStatusException;

import lombok.extern.slf4j.Slf4j;

/**
 * Class that drives what attributes should be returned to the client after a exception occurs.
 * This class extends the <code>DefaultErrorAttributes</code> and adds custom attributes
 * 
 * @author nischaltopno
 *
 */

@Component
@Slf4j
public class ServiceErrorAttributes extends DefaultErrorAttributes {

	@Override
	public Map<String, Object> getErrorAttributes(ServerRequest request, boolean includeStackTrace) {
		Map<String, Object> map = super.getErrorAttributes(request, includeStackTrace);

		Throwable e = getError(request);
		log.error("handling exception: ", e);

		if (e instanceof ResponseStatusException) {
			log.error("response for known exception");
			ResponseStatusException ex = (ResponseStatusException) e;
			map.put("exception", ex.getClass().getSimpleName());
			map.put("message", ex.getMessage());
			map.put("status", ex.getStatus().value());
			map.put("error", ex.getStatus().getReasonPhrase());

			return map;
		}

		log.error("response for unknown exception");
		map.put("exception", "SystemException");
		map.put("message", "System Error , Check logs!");
		map.put("status", "500");
		map.put("error", " System Error ");
		return map;
	}
}
