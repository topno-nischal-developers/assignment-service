package com.dbs.assignment.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Domain object to hold the details of a File
 * 
 * @author nischaltopno
 *
 */

@Getter
@Setter
@Builder
public class FileDetail {
	
	private String fullName;
	
	private boolean readable;
	
	private boolean executable;
	
	private boolean writeable;
	
	private String canonicalPath;
	
	private String parent;
	
	private long lenght;

}
