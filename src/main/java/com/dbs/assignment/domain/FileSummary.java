package com.dbs.assignment.domain;

import java.util.ArrayList;
import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Domain object to hold the list of files and folders of a directory recursively
 * 
 * @author nischaltopno
 *
 */

@Getter
@Setter
@Builder
public class FileSummary {

	private String fullName;
	
	private String type;
	
	private long size;
	
	@Builder.Default
	private List<FileSummary> childrens = new ArrayList<>();
}
