package com.dbs.assignment.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.dbs.assignment.domain.FileDetail;
import com.dbs.assignment.domain.FileSummary;
import com.dbs.assignment.exception.HttpResponseException;

import lombok.extern.slf4j.Slf4j;

/**
 * Service class to handle the business logic for file related API request
 * 
 * @author nischaltopno
 *
 */

@Service
@Slf4j
public class FileService {
	
	/**
	 * Retrieves the list of files and folders recursively.
	 * 
	 * @param dir
	 *        The directory for which the contents to be retrieved
	 * @return <code>FileSummary</code>
	 *         Object containing the list of files and folders
	 * @throws HttpResponseException
	 */
	public FileSummary getFiles(String dir) throws HttpResponseException {
		log.info("getFiles start for dir: {}", dir);
		Path path = Paths.get(dir);
		checkFileExists(path);
		checkIsDirectory(path);
		try {
			//TODO: Check options to use CompletableFuture which will make the API truly reactive
			return getFilesInPath(path);
		} catch (IOException e) {
			log.error("error while getting details of file: {}", dir, e);
			throw new HttpResponseException(HttpStatus.INTERNAL_SERVER_ERROR, "error while processing");
		}
	}
	
	/**
	 * Retrieves the details of a file
	 * 
	 * @param fileName
	 *        The file for which details needs to be retrieved
	 * @return <code>FileDetail</code>
	 *         The details of the file
	 * @throws HttpResponseException
	 */
	public FileDetail getFileDetail(String fileName) throws HttpResponseException {
		log.info("getFileDetail start for file: {}", fileName);
		Path path = Paths.get(fileName);
		checkFileExists(path);
		checkIsFile(path);
		
		File file = path.toFile();
		try {
			return FileDetail.builder()
				.fullName(file.getAbsolutePath())
				.readable(file.canRead())
				.executable(file.canExecute())
				.writeable(file.canWrite())
				.canonicalPath(file.getCanonicalPath())
				.parent(file.getParent())
				.lenght(file.length())
				.build();
		} catch (IOException e) {
			log.error("error while getting details of file: {}", fileName, e);
			throw new HttpResponseException(HttpStatus.INTERNAL_SERVER_ERROR, "error while processing");
		}
				
	}
	
	private FileSummary getFilesInPath(Path path) throws IOException {
		log.debug("getFilesInPath for file {}", path);
		FileSummary fileSummary = FileSummary.builder().build();
		
		fileSummary.setFullName(path.toFile().getAbsolutePath());
		if (Files.isDirectory(path)) {
			fileSummary.setSize(0);
			fileSummary.setType("dir");
			List<FileSummary> childrens = fileSummary.getChildrens();
			Files.list(path).forEach(c -> {
				try {
					childrens.add(getFilesInPath(c));
				} catch (IOException e) {
					log.error("error while getting details of file: {}", c, e);
				}
			});
		} else {
			fileSummary.setSize(Files.size(path));
			fileSummary.setType("file");
		}
		return fileSummary;
	}
	
	private void checkFileExists(Path path) {
		log.info("checkFileExists start");
		boolean fileExists;
		try {
			fileExists = Files.exists(path);
		} catch (SecurityException se) {
			log.error("error while checking existence of file: {}", path, se);
			throw new HttpResponseException(HttpStatus.FORBIDDEN, "forbidden");
		}
		if (!fileExists) {
			throw new HttpResponseException(HttpStatus.NOT_FOUND, "file not found");
		}
		log.info("checkFileExists end");
	}
	
	private void checkIsDirectory(Path path) {
		log.info("checkIsDirectory start");
		boolean isDirectory;
		try {
			isDirectory = Files.isDirectory(path);
		} catch (SecurityException se) {
			log.error("error while checking if direcotry or not: {}", path, se);
			throw new HttpResponseException(HttpStatus.FORBIDDEN, "forbidden");
		}
		if (!isDirectory) {
			throw new HttpResponseException(HttpStatus.BAD_REQUEST, "not a directory");
		}
		log.info("checkIsDirectory end");
	}
	
	private void checkIsFile(Path path) {
		log.info("checkIsFile start");
		boolean isDirectory;
		try {
			isDirectory = Files.isDirectory(path);
		} catch (SecurityException se) {
			log.error("error while checking if direcotry or not: {}", path, se);
			throw new HttpResponseException(HttpStatus.FORBIDDEN, "forbidden");
		}
		if (isDirectory) {
			throw new HttpResponseException(HttpStatus.BAD_REQUEST, "not a file");
		}
		log.info("checkIsFile end");
	}

}
